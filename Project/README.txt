How to run - on Ubuntu:

1. Download Mozilla code from https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/Source_Code/Downloading_Source_Archives
2. Replace the folder ./media/webrtc with the extracted folder from webrtc-mozilla.zip.
3. Run ./mach build in root of the mozilla code.
4. Run ./mach run in root of the mozilla code.
3. Extract folders from webrtc-client.zip.
4. Run npm install in folder server.
5. Run npm start in folder server.
6. Open the mozilla browser at localhost:8080.