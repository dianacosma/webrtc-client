import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import { FlexLayoutModule } from "@angular/flex-layout";

import 'hammerjs';

import { routes } from "./app.router";

import { AppComponent } from './app.component';

//layout
import { NavbarComponent } from "./components/layout/navbar/navbar.component";
import FooterComponent from "./components/layout/footer/footer.component";
import Divider from "./components/layout/divider/divider.component";

//common
import Loading from "./components/common/loading/loading.component";
import Chat from "./components/common/chat/chat.component";
import Video from "./components/common/video/video.component";
import SearchUsers from "./components/common/search-users/search-users.component";
import SearchRooms from "./components/common/search-rooms/search-rooms.component";

//pages
import { MainPage } from "./components/pages/main/main.page";
import HomePage from "./components/pages/home/home.page";

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    Divider,
    Loading,
    Chat,
    Video,
    SearchUsers,
    SearchRooms,

    MainPage,
    HomePage
  ],
  entryComponents: [
    SearchUsers,
    SearchRooms
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot(),
    FlexLayoutModule.forRoot(),
    routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
