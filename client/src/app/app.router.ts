import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AppComponent } from './app.component';

//layout
import { NavbarComponent } from "./components/layout/navbar/navbar.component";
import FooterComponent from "./components/layout/footer/footer.component";

//pages
import { MainPage } from "./components/pages/main/main.page";
import HomePage from "./components/pages/home/home.page";


export const router: Routes = [
    { path: "", redirectTo: "main", pathMatch: "full" },
    { path: "main", component: MainPage },
    { path: "home", component: HomePage }
];

export const routes: ModuleWithProviders = RouterModule.forRoot(router, { useHash: true });