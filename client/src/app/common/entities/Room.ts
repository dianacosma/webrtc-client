export default class Room {
    public name: String;
    public isPrivate: Boolean;

    public constructor() {
        this.name = "";
        this.isPrivate = false;
    }
}