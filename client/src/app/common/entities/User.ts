export default class User {
    public id: number;
    public username: string;
    public password: string;
    public color: string;

    public constructor() {
        this.id = -1;
        this.username = "";
        this.password = "";
        this.color = "#000";
    }
}