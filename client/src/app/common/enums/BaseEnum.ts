abstract class BaseEnum {
    public getAllPropValues(obj: any): Array<string> {
        let propValues = new Array<string>();

        for (let prop in obj) {
            propValues.push(String(obj[prop]));
        }

        return propValues;
    }
}

export default BaseEnum;