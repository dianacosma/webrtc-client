class RTCPeerConnection {
    private events: Map<string, any>;


    constructor() {
        this.events = new Map<string, Array<any>>();
    }

    on(key, action) {
        if (this.events.has(key)) {
            let actions = this.events.get(key);
            actions.push(action);
            this.events.set(key, actions);
        }
        else {
            this.events.set(key, new Array(action));
        }
    }

    emit(key, args) {
        let firedEventActions = this.events.get(key);

        firedEventActions.forEach(element => {
            element(args);
        });
    }
}

let instance = new RTCPeerConnection();

export default instance;