import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import BaseUrl from "../enums/BaseUrl";
import Room from "../entities/Room";

@Injectable()
export default class RoomService {
    public room: Room;
    public rooms: Array<Room>;
    constructor(private http: Http) {
        this.room = null;
        this.rooms = new Array<Room>();
    }

    public newRoom(name: String, isPrivate: Boolean): Observable<Room> {
        return this.addRoom(name, isPrivate).map((jsonData) => {
            let room = new Room();
            if (jsonData[0]) {
                room.name = jsonData[0].name;
                room.isPrivate = jsonData[0].isPrivate;
                return room;
            }
            return null;
        });
    }

    public getNonPrivateRooms(): Observable<Array<Room>> {
        return this.getAll().map((jsonData) => {
            let rooms = new Array<Room>();
            jsonData.forEach(element => {
                let room = new Room();
                room.name = element.room;
                room.isPrivate = element.isPrivate;
                rooms.push(room);
            });
            return rooms;
        });
    }

    public getRoom(name: String): Observable<Room> {
        return this.getOne(name).map((jsonData) => {
            let room = new Room();
            room.name = jsonData.room;
            room.isPrivate = jsonData.isPrivate;
            console.log(room);
            return room;
        });
    }

    private getAll() {
        return this.http.get(`http://localhost:8081/allRooms`)
            .map((data) => {
                console.log(data);
                return data.json();
            });
    }

    private getOne(name: String) {
        return this.http.get(`http://localhost:8081/room/${name}`)
            .map((data) => {
                return data.json();
            });
    }

    private addRoom(room: String, isPrivate: Boolean) {
        return this.http.post(`http://localhost:8081/newRoom`, { room, isPrivate })
            .map((data) => {
                console.log(data);
                return data.json();
            });
    }
}