import { Injectable } from "@angular/core";
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import BaseUrl from "../enums/BaseUrl";
import User from "../entities/User";

@Injectable()
export default class UserService {
    public loggedUser: User;

    constructor(private http: Http) {
        this.loggedUser = null;
    }

    public loginUser(username: string, password: string): Observable<User> {
        return this.getUser(username, password).map((jsonData) => {
            this.loggedUser = new User();
            if (jsonData[0]) {

                this.loggedUser.username = jsonData[0].username;
                this.loggedUser.password = jsonData[0].password;
                this.loggedUser.color = `#${jsonData[0].color}`;
                window.localStorage.setItem("user", this.loggedUser.username);
            }
            return this.loggedUser;
        });
    }

    public getUsers(): Observable<Array<User>> {
        return this.getAll().map((jsonData) => {
            let users = new Array<User>();
            jsonData.forEach(element => {
                let user = new User();
                user.username = element.username;
                user.password = element.password;
                user.color = `#${element.color}`;
                users.push(user);
            });
            return users;
        });
    }

    public getSingleUser(username): Observable<User> {
        return this.getOneUser(username).map((jsonData) => {
            let user = new User();
            if (jsonData[0]) {
                user.username = jsonData[0].username;
                user.color = `#${jsonData[0].color}`;
                return user;
            }
            return null;
        });
    }

    public registerUser(username: string, password: string): Observable<User> {
        var color = '000';
        return this.addUser(username, password, color).map((jsonData) => {
            let user = new User();
            if (jsonData[0]) {
                user.username = jsonData[0].username;
                user.password = jsonData[0].password;
                user.color = jsonData[0].color;
                return user;
            }
            return null;
        });
    }

    private getUser(username: string, password: string) {
        return this.http.post(`http://localhost:8081/login`, { username, password })
            .map((data) => {
                console.log(data);
                return data.json();
            });
    }

    private getOneUser(username: string) {
        return this.http.post(`http://localhost:8081/user`, { username })
            .map((data) => {
                console.log(data);
                return data.json();
            });
    }

    private getAll() {
        return this.http.get(`http://localhost:8081/allUsers`)
            .map((data) => {
                //console.log(data);
                return data.json();
            });
    }

    private addUser(username: string, password: string, color: string) {
        return this.http.post(`http://localhost:8081/register`, { username, password, color })
            .map((data) => {
                console.log(data);
                return data.json();
            });
    }
}