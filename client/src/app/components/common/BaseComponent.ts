import Events from "../../common/helpers/EventEmitter";

export default class BaseComponent {
    private customEventListener: Map<string, any>;

    constructor() {
        this.customEventListener = new Map();
    }

    listen(key: string, action: any) {
        Events.on(key, action);

        this.customEventListener.set(key, action);
    }

    emit(key, ...args) {
        Events.emit(key, args);
    }

    ngOnDestroy() {
        this.customEventListener.forEach((value: any, key: string) => {
            // Events.emitter.off(key, value);
        });
    }
}