import { Component, Input } from "@angular/core";

@Component({
    selector: "chat",
    templateUrl: "./chat.component.html",
    styleUrls: ["./chat.component.scss"]
})
export default class Chat {
    private message: String;
    private isConnection: String;
    private loggedUser: String;

    constructor() {
        this.init();
    }

    init(): void {
        this.loggedUser = window.localStorage.getItem("user");
        this.isConnection = window.localStorage.getItem("isConnection");
        if (!this.isConnection || this.isConnection == "false") {
            RTCInit();
        }
    }

    public sendMessage(): void {
        sendMessage(this.message, this.loggedUser);
    }

    public shareFile(): void {
        shareFile();
    }
}