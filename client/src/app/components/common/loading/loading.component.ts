import { Component } from "@angular/core";

import BaseComponent from "../BaseComponent";

@Component({
    selector: "loading",
    templateUrl: "./loading.component.html",
    styleUrls: ["./loading.component.scss"]
})
export default class Loading extends BaseComponent {
    private isHidden: boolean;

    constructor() {
        super();

        this.isHidden = true;

        this.initListener();
    }

    initListener() {
        super.listen("start-spinning", () => {
            this.isHidden = false;
        });

        super.listen("stop-spinning", () => {
            this.isHidden = true;
        });
    }
}