import { Component } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

import Room from "../../../common/entities/Room";
import RoomService from "../../../common/services/RoomService";

@Component({
  selector: 'search-rooms-dialog',
  templateUrl: 'search-rooms.component.html',
  styleUrls: ["./search-rooms.component.scss"],
  providers: [RoomService]
})
export default class SearchRooms {
  searchData: any;
  rooms: Array<Room>;

  constructor(private roomService: RoomService, public dialogRef: MdDialogRef<SearchRooms>) {
    this.init();
    this.getRooms();
  }

  public init(): void {
    this.searchData = '';
    this.rooms = [];
  }

  private getRoom(): Room {
    this.roomService.getRoom(this.searchData).subscribe(data => {
      console.log(data);
      this.rooms = [];
      this.rooms.push(data)
    },
      errorData => {
        console.log(errorData);
      });
    return new Room();
  }

  private getRooms() {
    this.roomService.getNonPrivateRooms().subscribe(data => {
      this.rooms = data;
      console.log(data);
    },
      errorData => {
        console.log(errorData);
      });
    return new Array<Room>();
  }

}