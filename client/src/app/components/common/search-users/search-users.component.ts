import { Component } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

import User from "../../../common/entities/User";
import UserService from "../../../common/services/UserService";

@Component({
  selector: 'search-users-dialog',
  templateUrl: 'search-users.component.html',
  styleUrls: ["./search-users.component.scss"],
  providers: [UserService]
})
export default class SearchUsers {
  searchData: any;
  users: Array<User>;

  constructor(private userService: UserService, public dialogRef: MdDialogRef<SearchUsers>) {
    this.init();
    this.getUsers();
  }

  public init(): void {
    this.searchData = '';
    this.users = [];
  }

  private getUser(): User {
    this.userService.getSingleUser(this.searchData).subscribe(data => {
      console.log(data);
      this.users = [];
      this.users.push(data)
    },
      errorData => {
        console.log(errorData);
      });
    return new User();
  }

  private getUsers(): Array<User> {
    this.users = [];
    this.userService.getUsers().subscribe(data => {
      this.users = data;
      console.log(data);
    },
      errorData => {
        console.log(errorData);
      });
    return new Array<User>();
  }
}