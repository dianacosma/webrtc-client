import { Component } from "@angular/core";

@Component({
    selector: "footer",
    templateUrl: "./footer.component.html",
    styleUrls: ["./footer.component.scss"]
})
export default class FooterComponent {
    private cssValidationImageUrl: string;
    private cssValidationUrl: string;

    constructor() {
        this.cssValidationImageUrl = "http://jigsaw.w3.org/css-validator/images/vcss-blue";
        this.cssValidationUrl = "http://jigsaw.w3.org/css-validator/check/referer";
    }
}