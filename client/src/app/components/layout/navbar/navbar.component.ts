import { Component } from "@angular/core";
import { MdDialog, MdDialogRef } from '@angular/material';
import SearchUsers from '../../common/search-users/search-users.component';
import SearchRooms from '../../common/search-rooms/search-rooms.component';
import UserService from '../../../common/services/UserService';

@Component({
    selector: "navbar",
    templateUrl: "./navbar.component.html",
    styleUrls: ['./navbar.component.scss'],
    providers: [UserService, SearchUsers]
})
export class NavbarComponent {
    public username: String;
    private loggedUser: String;

    constructor(public userService: UserService, public dialog: MdDialog) {
       this.loggedUser = window.localStorage.getItem("user")
    }

    openUserDialog() {
        let dialogRef = this.dialog.open(SearchUsers);
        dialogRef.afterClosed().subscribe(result => {

        });
    }

    openRoomDialog() {
        let dialogRef = this.dialog.open(SearchRooms);
        dialogRef.afterClosed().subscribe(result => {

        });
    }
}