import BaseComponent from "../common/BaseComponent";

export default class BasePageComponent extends BaseComponent {
    private _isRequestProcessing: boolean;
    
    constructor() {
        super();

        this._isRequestProcessing = false;
    }

    set isRequestProcessing(value: boolean) {
        this._isRequestProcessing = value;

        if (this._isRequestProcessing) {
            super.emit("start-spinning");
        } else {
            super.emit("stop-spinning");
        }
    }

    get isRequestProcessing(): boolean {
        return this._isRequestProcessing;
    }
}