import { Component } from "@angular/core";
import { MdSnackBar } from "@angular/material";
import RoomService from "../../../common/services/RoomService";
import UserService from "../../../common/services/UserService";

@Component({
    selector: "home",
    templateUrl: "./home.page.html",
    styleUrls: ["./home.page.scss"],
    providers: [RoomService, UserService]
})
export default class HomePage {
    private roomName: String;
    private loggedUser: String;
    private isRTCConnectionOpen: boolean;
    private isPrivate: boolean;

    constructor(private userService: UserService, private roomService: RoomService, private snackBar: MdSnackBar) {
        this.init();
    }

    public init(): void {
        this.loggedUser = window.localStorage.getItem("user");
        this.isRTCConnectionOpen = false;
        window.localStorage.setItem("isConnection", "false");
        this.isPrivate = false;
    }

    public openRoom(): void {
        console.log(this.roomName);
        this.roomService.getRoom(this.roomName)
            .subscribe(data => {
                console.log(data);
                this.snackBar.open("There is already a room with this name. Please insert another one!",
                    "",
                    {
                        duration: 3000
                    });
            },
            errorData => {
                console.log(errorData);
                this.open();
            });
    }

    public joinRoom(): void {
        console.log(this.roomName);
        this.roomService.getRoom(this.roomName)
            .subscribe(data => {
                console.log(data);
                this.join();
            },
            errorData => {
                console.log(errorData);
                this.snackBar.open("The room you are trying to reach does not exist. Please insert another one!",
                    "",
                    {
                        duration: 3000
                    });
            });
    }

    private open(): void {
        if (!this.isRTCConnectionOpen) {
            RTCInit();
            this.isRTCConnectionOpen = true;
            window.localStorage.setItem("isConnection", "true");
        }
        this.roomName = this.strip(this.roomName);

        if (!this.roomName) {
            this.snackBar.open("Unsuitable room name!",
                "",
                {
                    duration: 3000
                });
            return;
        }
        this.roomService.newRoom(this.roomName, this.isPrivate)
            .subscribe(data => {
                openRtcRoom(this.roomName, this.loggedUser);
            },
            errorData => {
                this.snackBar.open("Something went wrong. Please try again!",
                    "",
                    {
                        duration: 3000
                    });
            });
    }

    private join(): void {
        if (!this.isRTCConnectionOpen) {
            RTCInit();
            this.isRTCConnectionOpen = true;
            window.localStorage.setItem("isConnection", "true");
        }
        this.roomName = this.strip(this.roomName);

        if (!this.roomName) {
            this.snackBar.open("Unsuitable room name!",
                "",
                {
                    duration: 3000
                });
            return;
        }

        joinRtcRoom(this.roomName, this.loggedUser);
    }

    private strip(value: String): String {
        return value.replace(/^\s+|\s+$/g, '');
    }
}