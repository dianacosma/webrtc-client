import { Component } from "@angular/core";
import { MdSnackBar } from "@angular/material";
import { Router } from '@angular/router';

import UserService from "../../../common/services/UserService";

@Component({
    selector: "main-page",
    templateUrl: "./main.page.html",
    styleUrls: ["./main.page.scss"],
    providers: [UserService]
})
export class MainPage {
    showActiveClass: false;
    loginData: any;
    registerData: any;

    constructor(private userService: UserService, private snackBar: MdSnackBar, private router: Router) {
        this.init();
    }

    getStyle() {
        if (this.showActiveClass) {
            return "active";
        } else {
            return "";
        }
    }
    public init(): void {
        this.loginData = {};
        this.registerData = {};
    }

    public login(): void {
        this.userService.loginUser(this.loginData.username, this.loginData.password).subscribe(data => {
            this.snackBar.open("You have logged in successfully!",
                "",
                {
                    duration: 3000
                });

            this.router.navigate(["/home"]);
        },
            errorData => {
                this.snackBar.open("Something went wrong. Please try again!",
                    "",
                    {
                        duration: 3000
                    });
            });
    }

    public register(): void {
        if (this.registerData.password !== this.registerData.repeatPassword) {
            this.snackBar.open("Passwords do not match. Please try again!",
                "",
                {
                    duration: 3000
                });
            return;
        }
        this.userService.registerUser(this.registerData.username, this.registerData.password).subscribe(data => {
            this.snackBar.open("You have logged in successfully!",
                "",
                {
                    duration: 3000
                });

            this.router.navigate(["/main"]);
        },
            errorData => {
                this.snackBar.open("Something went wrong. Please try again!",
                    "",
                    {
                        duration: 3000
                    });
            });
    }
}