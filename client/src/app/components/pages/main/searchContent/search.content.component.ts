import {Component} from "@angular/core";
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: "search-content",
    templateUrl: "./search.content.component.html",
    styleUrls: ["./search.content.component.scss"]
})
export class SearchContentComponent {
    searchModel: string = "";

    constructor(private router: Router) {
    }

    public onSearchClick(): void {
        this.router.navigate(["/search-results", this.searchModel]);
    }
}