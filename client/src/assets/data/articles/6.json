{
    "id": 5,
    "name": "Intel",
    "generalInformation": "Intel Corporation (also known as Intel, stylized as intel) is an American multinational corporation and technology company headquartered in Santa Clara, California (colloquially referred to as \"Silicon Valley\") that was founded by Gordon Moore (of Moore's law fame) and Robert Noyce. It is the world's largest and highest valued semiconductor chip makers based on revenue,[2] and is the inventor of the x86 series of microprocessors: the processors found in most personal computers (PCs). Intel supplies processors for computer system manufacturers such as Apple, Lenovo, HP, and Dell. Intel also manufactures motherboard chipsets, network interface controllers and integrated circuits, flash memory, graphics chips, embedded processors and other devices related to communications and computing.",
    "image": {
        "path": "./assets/img/articles/intel.png",
        "alt": "Intel logo",
        "caption": "Intel's current logo, used since 2006"
    },
    "sections": [
        {
            "name": "Current operations",
            "data": "",
            "subsections": [
                {
                    "name": "`Operating segments",
                    "data": "<ul><li><b>Client Computing Group</b> - 55% of 2016 revenues - produces hardware components used in desktop and notebook computers</li><li><b>Data Center Group</b> - 29% of 2016 revenues - produces hardware components used in server, network, and storage platforms</li><li></b>Internet of Things Group</b> - 5% of 2016 revenues - offers platforms designed for retail, transportation, industrial, buildings and home use</li><li><b>Non-Valtile Memory Solutions Group</b> - 4% of 2016 revenues</li><li><b>Intel Security Group</b> - 4% of 2016 revenues</li><li><b>Programmable Solutions Group</b> - 3% of 2016 revenues</li></ul>"
                },
                {
                    "name": "Top customers",
                    "data": "In 2016, Dell accounted for 15% of Intel's total revenues, Lenovo accounted for 13% of total revenues, and HP Inc. accounted for 10% of total revenues."
                },
                {
                    "name": "Market share",
                    "data": "According to IDC, while Intel enjoyed the biggest market share in both the overall worldwide PC microprocessor market (79.3%) and the mobile PC microprocessor (84.4%) in the second quarter of 2011, the numbers decreased by 1.5% and 1.9% compared to the first quarter of 2011. <br/><br/> n the 1980s, Intel was among the top ten sellers of semiconductors (10th in 1987) in the world. In 1991, Intel became the biggest chip maker by revenue and has held the position ever since. Other top semiconductor companies include TSMC, Advanced Micro Devices, Samsung, Texas Instruments, Toshiba and STMicroelectronics. <br/><br/> Competitors in PC chip sets include Advanced Micro Devices, VIA Technologies, Silicon Integrated Systems, and Nvidia. Intel's competitors in networking include NXP Semiconductors, Infineon, Broadcom Limited, Marvell Technology Group and Applied Micro Circuits Corporation, and competitors in flash memory include Spansion, Samsung, Qimonda, Toshiba, STMicroelectronics, and SK Hynix."
                }
            ]
        },
        {
            "name": "Corporate history",
            "data": "",
            "subsections": [
                {
                    "name": "Origins",
                    "data": "Intel was founded in Mountain View, California in 1968 by Gordon E. Moore (of \"Moore's law\" fame), a chemist, and Robert Noyce, a physicist and co-inventor of the integrated circuit. Arthur Rock (investor and venture capitalist) helped them find investors, while Max Palevsky was on the board from an early stage.[14] Moore and Noyce had left Fairchild Semiconductor to found Intel. Rock was not an employee, but he was an investor and was chairman of the board.[15][16] The total initial investment in Intel was $2.5 million convertible debentures and $10,000 from Rock. Just 2 years later, Intel became a public company via an initial public offering (IPO), raising $6.8 million ($23.50 per share).[15] Intel's third employee was Andy Grove,[17] a chemical engineer, who later ran the company through much of the 1980s and the high-growth 1990s."
                },
                {
                    "name": "Early history",
                    "data": "At its founding, Intel was distinguished by its ability to make semiconductors. Its first product, in 1969, was the 3101 Schottky TTL bipolar 64-bit static random-access memory (SRAM), which was nearly twice as fast as earlier Schottky diode implementations by Fairchild and the Electrotechnical Laboratory in Tsukuba, Japan.[21][22] In the same year, Intel also produced the 3301 Schottky bipolar 1024-bit read-only memory (ROM)[23] and the first commercial metal–oxide–semiconductor field-effect transistor (MOSFET) silicon gate SRAM chip, the 256-bit 1101.[15][24][25] Intel's business grew during the 1970s as it expanded and improved its manufacturing processes and produced a wider range of products, still dominated by various memory devices."
                },
                {
                    "name": "Slowing demand and challenges to dominance in 2000",
                    "data": "After 2000, growth in demand for high-end microprocessors slowed. Competitors, notably AMD (Intel's largest competitor in its primary x86 architecture market), garnered significant market share, initially in low-end and mid-range processors but ultimately across the product range, and Intel's dominant position in its core market was greatly reduced.[28] In the early 2000s then-CEO, Craig Barrett attempted to diversify the company's business beyond semiconductors, but few of these activities were ultimately successful."
                },
                {
                    "name": "Acquisitions (2010–2017)",
                    "data": "In 2010, Intel purchased McAfee, a manufacturer of computer security technology for $7.68 billion.[36] As a condition for regulatory approval of the transaction, Intel agreed to provide rival security firms with all necessary information that would allow their products to use Intel's chips and personal computers.[37] After the acquisition, Intel had about 90,000 employees, including about 12,000 software engineers.[38] In September 2016, Intel sold a majority stake in its computer-security unit to TPG, reversing the five-year-old McAfee acquisition. <br/><br/> In March 2011, Intel bought most of the assets of Cairo-based SySDSoft. In July 2011, Intel announced that it had agreed to acquire Fulcrum Microsystems Inc., a company specializing in network switches.[43] The company used to be included on the EE Times list of 60 Emerging Startups. <br/><br/> The acquisition of a Spanish natural language recognition startup, Indisys was announced in September 2013. The terms of the deal were not disclosed but an email from an Intel representative stated: \"Intel has acquired Indisys, a privately held company based in Seville, Spain. The majority of Indisys employees joined Intel. We signed the agreement to acquire the company on May 31 and the deal has been completed.\" Indysis explains that its artificial intelligence (AI) technology \"is a human image, which converses fluently and with common sense in multiple languages and also works in different platforms.\" <br/><br/> In March 2017, Intel announced that they had agreed a US$15.3 billion takeover of Mobileye, an Israeli developer of \"autonomous driving\" systems."
                }
            ]
        },
        {
            "name": "Product and market history",
            "data": "",
            "subsections": [
                {
                    "name": "SRAMS and the microprocessor",
                    "data": "Intel's first products were shift register memory and random-access memory integrated circuits, and Intel grew to be a leader in the fiercely competitive DRAM, SRAM, and ROM markets throughout the 1970s. Concurrently, Intel engineers Marcian Hoff, Federico Faggin, Stanley Mazor and Masatoshi Shima invented Intel's first microprocessor. Originally developed for the Japanese company Busicom to replace a number of ASICs in a calculator already produced by Busicom, the Intel 4004 was introduced to the mass market on November 15, 1971, though the microprocessor did not become the core of Intel's business until the mid-1980s. (Note: Intel is usually given credit with Texas Instruments for the almost-simultaneous invention of the microprocessor)"
                },
                {
                    "name": "Intel, x86 processors, and the IBM PC",
                    "data": "Despite the ultimate importance of the microprocessor, the 4004 and its successors the 8008 and the 8080 were never major revenue contributors at Intel. As the next processor, the 8086 (and its variant the 8088) was completed in 1978, Intel embarked on a major marketing and sales campaign for that chip nicknamed \"Operation Crush\", and intended to win as many customers for the processor as possible. One design win was the newly created IBM PC division, though the importance of this was not fully realized at the time. <br/><br/> IBM introduced its personal computer in 1981, and it was rapidly successful. In 1982, Intel created the 80286 microprocessor, which, two years later, was used in the IBM PC/AT. Compaq, the first IBM PC \"clone\" manufacturer, produced a desktop system based on the faster 80286 processor in 1985 and in 1986 quickly followed with the first 80386-based system, beating IBM and establishing a competitive market for PC-compatible systems and setting up Intel as a key component supplier."
                },
                {
                    "name": "Solid-state drives (SSD)",
                    "data": "On September 8, 2008, Intel began shipping its first mainstream solid-state drives, the X18-M and X25-M with 80 GB and 160 GB storage capacities.[96] Reviews measured high performance with these MLC-based drives. Intel released its SLC-based Enterprise X25-E Extreme SSDs on October 15 that same year in capacities of 32GB and 64GB. <br/><br/> On February 1, 2010, Intel and Micron announced that they were gearing up for production of NAND flash memory using a new 25-nanometer process.[105] In March of that same year, Intel entered the budget SSD segment with its X25-V drives with an initial capacity of 40 GB.[106] The SSD 310, Intel's first mSATA drive was released on December 2010, providing X25-M G2 performance in a much smaller package."
                },
                {
                    "name": "Introduction of Ivy Bridge 22 nm processors (2011)",
                    "data": "In 2011, Intel announced the Ivy Bridge processor family at the Intel DeveConflict FreeCloper Forum.[182] Ivy Bridge supports both DDR3 memory and DDR3L chips."
                }
            ]
        }
    ]
}