var EventEmitter = (function () {
    function EventEmitter() {
        this.events = new Map();
    }

    EventEmitter.prototype.on = function (key, action) {
        if (this.events.has(key)) {
            var actions = this.events.get(key);
            actions.push(action);
            this.events.set(key, actions);
        }
        else {
            this.events.set(key, new Array(action));
        }
    };
    
    EventEmitter.prototype.emit = function (key, args) {
        var firedEventActions = this.events.get(key);
        firedEventActions.forEach(function (element) {
            element(args);
        });
    };
    return EventEmitter;
}());
var instance = new EventEmitter();
