var connection = null;
var signaler = null;
function RTCInit() {
    connection = new RTCMultiConnection();

    connection.body = document.getElementById('videos-container');
    connection.chatBody = document.getElementById('chat-output');

    signaler = initReliableSignaler(connection, '/');
    connection.session = {
        audio: false,
        video: true,
        data: true
    };
    connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: false,
        OfferToReceiveVideo: true
    };
    var videoConstraints = {
        mandatory: {
            maxWidth: 1920,
            maxHeight: 1080,
            minAspectRatio: 1.77,
            minFrameRate: 3,
            maxFrameRate: 64
        },
        optional: []
    };
    var audioConstraints = {
        mandatory: {
            // echoCancellation: false,
            // googEchoCancellation: false, // disabling audio processing
            // googAutoGainControl: true,
            // googNoiseSuppression: true,
            // googHighpassFilter: true,
            // googTypingNoiseDetection: true,
            // googAudioMirroring: true
        },
        optional: []
    };
    connection.mediaConstraints = {
        video: videoConstraints,
        audio: audioConstraints
    };

    connection.onopen = function () {
        // document.getElementById('share-file').disabled = false;
        // document.getElementById('input-text-chat').disabled = false;
    };

    connection.onmessage = appendDIV;
    var chatContainer = document.getElementById('chat-output');
    function appendDIV(event) {
        if(typeof event.data === "object"){
            return;
        }
        var div = document.createElement('div');
        var user = document.createElement('p');
        // div.style.float = "left";
        div.style.width = "100%";
        user.classList.add('incoming-message');
        user.innerHTML = event.userid + ": ";
        var msg = document.createElement('p');
        msg.innerHTML = event.data || event;
        div.appendChild(user);
        div.appendChild(msg);
        chatContainer.appendChild(div);
        div.tabIndex = 0; div.focus();
    }
}

function openRtcRoom(roomid, username) {
    connection.channel = connection.sessionid = roomid;
    connection.userid = username;
    connection.open({
        onMediaCaptured: function () {
            signaler.createNewRoomOnServer(connection.roomid);
        }
    });
}

function joinRtcRoom(roomid, username) {
    signaler.getRoomFromServer(roomid, function (roomid) {
        connection.channel = connection.sessionid = roomid;
        connection.userid = username;
        connection.join({
            sessionid: roomid,
            extra: {},
            session: connection.session
        });
    });
}

function sendMessage(message, username) {
    addMessage(message, username);
    connection.send(message);
}

function shareFile() {
    var fileSelector = new FileSelector();
    fileSelector.selectSingleFile(function (file) {
        connection.send(file);
    });
}

function addMessage(message, username) {
    var chatContainer = document.getElementById('chat-output');
    var div = document.createElement('div');
    div.style.float = "right";
    div.style.width = "100%";
    var user = document.createElement('p');
    user.classList.add('outgoing-message');
    user.innerHTML = username + ": ";
    var msg = document.createElement('p');
    msg.innerHTML = message;
    div.appendChild(user);
    div.appendChild(msg);
    chatContainer.appendChild(div);
    div.tabIndex = 0; div.focus();
}