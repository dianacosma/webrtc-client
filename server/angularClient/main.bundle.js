webpackJsonp([1,4],{

/***/ 186:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entities_User__ = __webpack_require__(461);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserService = (function () {
    function UserService(http) {
        this.http = http;
        this.loggedUser = null;
    }
    UserService.prototype.loginUser = function (username, password) {
        var _this = this;
        return this.getUser(username, password).map(function (jsonData) {
            _this.loggedUser = new __WEBPACK_IMPORTED_MODULE_2__entities_User__["a" /* default */]();
            if (jsonData[0]) {
                _this.loggedUser.username = jsonData[0].username;
                _this.loggedUser.password = jsonData[0].password;
                _this.loggedUser.color = "#" + jsonData[0].color;
                window.localStorage.setItem("user", _this.loggedUser.username);
            }
            return _this.loggedUser;
        });
    };
    UserService.prototype.getUsers = function () {
        return this.getAll().map(function (jsonData) {
            var users = new Array();
            jsonData.forEach(function (element) {
                var user = new __WEBPACK_IMPORTED_MODULE_2__entities_User__["a" /* default */]();
                user.username = element.username;
                user.password = element.password;
                user.color = "#" + element.color;
                users.push(user);
            });
            return users;
        });
    };
    UserService.prototype.getSingleUser = function (username) {
        return this.getOneUser(username).map(function (jsonData) {
            var user = new __WEBPACK_IMPORTED_MODULE_2__entities_User__["a" /* default */]();
            if (jsonData[0]) {
                user.username = jsonData[0].username;
                user.color = "#" + jsonData[0].color;
                return user;
            }
            return null;
        });
    };
    UserService.prototype.registerUser = function (username, password) {
        var color = '000';
        return this.addUser(username, password, color).map(function (jsonData) {
            var user = new __WEBPACK_IMPORTED_MODULE_2__entities_User__["a" /* default */]();
            if (jsonData[0]) {
                user.username = jsonData[0].username;
                user.password = jsonData[0].password;
                user.color = jsonData[0].color;
                return user;
            }
            return null;
        });
    };
    UserService.prototype.getUser = function (username, password) {
        return this.http.post("http://localhost:8081/login", { username: username, password: password })
            .map(function (data) {
            console.log(data);
            return data.json();
        });
    };
    UserService.prototype.getOneUser = function (username) {
        return this.http.post("http://localhost:8081/user", { username: username })
            .map(function (data) {
            console.log(data);
            return data.json();
        });
    };
    UserService.prototype.getAll = function () {
        return this.http.get("http://localhost:8081/allUsers")
            .map(function (data) {
            //console.log(data);
            return data.json();
        });
    };
    UserService.prototype.addUser = function (username, password, color) {
        return this.http.post("http://localhost:8081/register", { username: username, password: password, color: color })
            .map(function (data) {
            console.log(data);
            return data.json();
        });
    };
    UserService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === 'function' && _a) || Object])
    ], UserService);
    return UserService;
    var _a;
}());
/* harmony default export */ __webpack_exports__["a"] = UserService;
//# sourceMappingURL=UserService.js.map

/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var Room = (function () {
    function Room() {
        this.name = "";
        this.isPrivate = false;
    }
    return Room;
}());
/* harmony default export */ __webpack_exports__["a"] = Room;
//# sourceMappingURL=Room.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var User = (function () {
    function User() {
        this.id = -1;
        this.username = "";
        this.password = "";
        this.color = "#000";
    }
    return User;
}());
/* harmony default export */ __webpack_exports__["a"] = User;
//# sourceMappingURL=User.js.map

/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entities_Room__ = __webpack_require__(460);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RoomService = (function () {
    function RoomService(http) {
        this.http = http;
        this.room = null;
        this.rooms = new Array();
    }
    RoomService.prototype.newRoom = function (name, isPrivate) {
        return this.addRoom(name, isPrivate).map(function (jsonData) {
            var room = new __WEBPACK_IMPORTED_MODULE_2__entities_Room__["a" /* default */]();
            if (jsonData[0]) {
                room.name = jsonData[0].name;
                room.isPrivate = jsonData[0].isPrivate;
                return room;
            }
            return null;
        });
    };
    RoomService.prototype.getNonPrivateRooms = function () {
        return this.getAll().map(function (jsonData) {
            var rooms = new Array();
            jsonData.forEach(function (element) {
                var room = new __WEBPACK_IMPORTED_MODULE_2__entities_Room__["a" /* default */]();
                room.name = element.room;
                room.isPrivate = element.isPrivate;
                rooms.push(room);
            });
            return rooms;
        });
    };
    RoomService.prototype.getRoom = function (name) {
        return this.getOne(name).map(function (jsonData) {
            var room = new __WEBPACK_IMPORTED_MODULE_2__entities_Room__["a" /* default */]();
            room.name = jsonData.room;
            room.isPrivate = jsonData.isPrivate;
            console.log(room);
            return room;
        });
    };
    RoomService.prototype.getAll = function () {
        return this.http.get("http://localhost:8081/allRooms")
            .map(function (data) {
            console.log(data);
            return data.json();
        });
    };
    RoomService.prototype.getOne = function (name) {
        return this.http.get("http://localhost:8081/room/" + name)
            .map(function (data) {
            return data.json();
        });
    };
    RoomService.prototype.addRoom = function (room, isPrivate) {
        return this.http.post("http://localhost:8081/newRoom", { room: room, isPrivate: isPrivate })
            .map(function (data) {
            console.log(data);
            return data.json();
        });
    };
    RoomService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["p" /* Injectable */])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === 'function' && _a) || Object])
    ], RoomService);
    return RoomService;
    var _a;
}());
/* harmony default export */ __webpack_exports__["a"] = RoomService;
//# sourceMappingURL=RoomService.js.map

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common_entities_Room__ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_services_RoomService__ = __webpack_require__(462);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchRooms = (function () {
    function SearchRooms(roomService, dialogRef) {
        this.roomService = roomService;
        this.dialogRef = dialogRef;
        this.init();
        this.getRooms();
    }
    SearchRooms.prototype.init = function () {
        this.searchData = '';
        this.rooms = [];
    };
    SearchRooms.prototype.getRoom = function () {
        var _this = this;
        this.roomService.getRoom(this.searchData).subscribe(function (data) {
            console.log(data);
            _this.rooms = [];
            _this.rooms.push(data);
        }, function (errorData) {
            console.log(errorData);
        });
        return new __WEBPACK_IMPORTED_MODULE_2__common_entities_Room__["a" /* default */]();
    };
    SearchRooms.prototype.getRooms = function () {
        var _this = this;
        this.roomService.getNonPrivateRooms().subscribe(function (data) {
            _this.rooms = data;
            console.log(data);
        }, function (errorData) {
            console.log(errorData);
        });
        return new Array();
    };
    SearchRooms = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: 'search-rooms-dialog',
            template: __webpack_require__(872),
            styles: [__webpack_require__(859)],
            providers: [__WEBPACK_IMPORTED_MODULE_3__common_services_RoomService__["a" /* default */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__common_services_RoomService__["a" /* default */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__common_services_RoomService__["a" /* default */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MdDialogRef */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MdDialogRef */]) === 'function' && _b) || Object])
    ], SearchRooms);
    return SearchRooms;
    var _a, _b;
}());
/* harmony default export */ __webpack_exports__["a"] = SearchRooms;
//# sourceMappingURL=search-rooms.component.js.map

/***/ }),

/***/ 464:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common_entities_User__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__ = __webpack_require__(186);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchUsers = (function () {
    function SearchUsers(userService, dialogRef) {
        this.userService = userService;
        this.dialogRef = dialogRef;
        this.init();
        this.getUsers();
    }
    SearchUsers.prototype.init = function () {
        this.searchData = '';
        this.users = [];
    };
    SearchUsers.prototype.getUser = function () {
        var _this = this;
        this.userService.getSingleUser(this.searchData).subscribe(function (data) {
            console.log(data);
            _this.users = [];
            _this.users.push(data);
        }, function (errorData) {
            console.log(errorData);
        });
        return new __WEBPACK_IMPORTED_MODULE_2__common_entities_User__["a" /* default */]();
    };
    SearchUsers.prototype.getUsers = function () {
        var _this = this;
        this.users = [];
        this.userService.getUsers().subscribe(function (data) {
            _this.users = data;
            console.log(data);
        }, function (errorData) {
            console.log(errorData);
        });
        return new Array();
    };
    SearchUsers = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: 'search-users-dialog',
            template: __webpack_require__(873),
            styles: [__webpack_require__(860)],
            providers: [__WEBPACK_IMPORTED_MODULE_3__common_services_UserService__["a" /* default */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__["a" /* default */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__["a" /* default */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MdDialogRef */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_material__["c" /* MdDialogRef */]) === 'function' && _b) || Object])
    ], SearchUsers);
    return SearchUsers;
    var _a, _b;
}());
/* harmony default export */ __webpack_exports__["a"] = SearchUsers;
//# sourceMappingURL=search-users.component.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common_services_RoomService__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__ = __webpack_require__(186);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = (function () {
    function HomePage(userService, roomService, snackBar) {
        this.userService = userService;
        this.roomService = roomService;
        this.snackBar = snackBar;
        this.init();
    }
    HomePage.prototype.init = function () {
        this.loggedUser = window.localStorage.getItem("user");
        this.isRTCConnectionOpen = false;
        window.localStorage.setItem("isConnection", "false");
        this.isPrivate = false;
    };
    HomePage.prototype.openRoom = function () {
        var _this = this;
        console.log(this.roomName);
        this.roomService.getRoom(this.roomName)
            .subscribe(function (data) {
            console.log(data);
            _this.snackBar.open("There is already a room with this name. Please insert another one!", "", {
                duration: 3000
            });
        }, function (errorData) {
            console.log(errorData);
            _this.open();
        });
    };
    HomePage.prototype.joinRoom = function () {
        var _this = this;
        console.log(this.roomName);
        this.roomService.getRoom(this.roomName)
            .subscribe(function (data) {
            console.log(data);
            _this.join();
        }, function (errorData) {
            console.log(errorData);
            _this.snackBar.open("The room you are trying to reach does not exist. Please insert another one!", "", {
                duration: 3000
            });
        });
    };
    HomePage.prototype.open = function () {
        var _this = this;
        if (!this.isRTCConnectionOpen) {
            RTCInit();
            this.isRTCConnectionOpen = true;
            window.localStorage.setItem("isConnection", "true");
        }
        this.roomName = this.strip(this.roomName);
        if (!this.roomName) {
            this.snackBar.open("Unsuitable room name!", "", {
                duration: 3000
            });
            return;
        }
        this.roomService.newRoom(this.roomName, this.isPrivate)
            .subscribe(function (data) {
            openRtcRoom(_this.roomName, _this.loggedUser);
        }, function (errorData) {
            _this.snackBar.open("Something went wrong. Please try again!", "", {
                duration: 3000
            });
        });
    };
    HomePage.prototype.join = function () {
        if (!this.isRTCConnectionOpen) {
            RTCInit();
            this.isRTCConnectionOpen = true;
            window.localStorage.setItem("isConnection", "true");
        }
        this.roomName = this.strip(this.roomName);
        if (!this.roomName) {
            this.snackBar.open("Unsuitable room name!", "", {
                duration: 3000
            });
            return;
        }
        joinRtcRoom(this.roomName, this.loggedUser);
    };
    HomePage.prototype.strip = function (value) {
        return value.replace(/^\s+|\s+$/g, '');
    };
    HomePage = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: "home",
            template: __webpack_require__(878),
            styles: [__webpack_require__(865)],
            providers: [__WEBPACK_IMPORTED_MODULE_2__common_services_RoomService__["a" /* default */], __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__["a" /* default */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__["a" /* default */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__["a" /* default */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__common_services_RoomService__["a" /* default */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__common_services_RoomService__["a" /* default */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MdSnackBar */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MdSnackBar */]) === 'function' && _c) || Object])
    ], HomePage);
    return HomePage;
    var _a, _b, _c;
}());
/* harmony default export */ __webpack_exports__["a"] = HomePage;
//# sourceMappingURL=home.page.js.map

/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(454);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__ = __webpack_require__(186);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainPage; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MainPage = (function () {
    function MainPage(userService, snackBar, router) {
        this.userService = userService;
        this.snackBar = snackBar;
        this.router = router;
        this.init();
    }
    MainPage.prototype.getStyle = function () {
        if (this.showActiveClass) {
            return "active";
        }
        else {
            return "";
        }
    };
    MainPage.prototype.init = function () {
        this.loginData = {};
        this.registerData = {};
    };
    MainPage.prototype.login = function () {
        var _this = this;
        this.userService.loginUser(this.loginData.username, this.loginData.password).subscribe(function (data) {
            _this.snackBar.open("You have logged in successfully!", "", {
                duration: 3000
            });
            _this.router.navigate(["/home"]);
        }, function (errorData) {
            _this.snackBar.open("Something went wrong. Please try again!", "", {
                duration: 3000
            });
        });
    };
    MainPage.prototype.register = function () {
        var _this = this;
        if (this.registerData.password !== this.registerData.repeatPassword) {
            this.snackBar.open("Passwords do not match. Please try again!", "", {
                duration: 3000
            });
            return;
        }
        this.userService.registerUser(this.registerData.username, this.registerData.password).subscribe(function (data) {
            _this.snackBar.open("You have logged in successfully!", "", {
                duration: 3000
            });
            _this.router.navigate(["/main"]);
        }, function (errorData) {
            _this.snackBar.open("Something went wrong. Please try again!", "", {
                duration: 3000
            });
        });
    };
    MainPage = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: "main-page",
            template: __webpack_require__(879),
            styles: [__webpack_require__(866)],
            providers: [__WEBPACK_IMPORTED_MODULE_3__common_services_UserService__["a" /* default */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__["a" /* default */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__common_services_UserService__["a" /* default */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MdSnackBar */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_material__["b" /* MdSnackBar */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === 'function' && _c) || Object])
    ], MainPage);
    return MainPage;
    var _a, _b, _c;
}());
//# sourceMappingURL=main.page.js.map

/***/ }),

/***/ 517:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 517;


/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(661);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(702);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app works!!!';
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__(869),
            styles: [__webpack_require__(856)]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_material__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__ = __webpack_require__(610);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_hammerjs__ = __webpack_require__(867);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_hammerjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__app_router__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(691);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_layout_navbar_navbar_component__ = __webpack_require__(701);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_layout_footer_footer_component__ = __webpack_require__(700);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_layout_divider_divider_component__ = __webpack_require__(699);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_common_loading_loading_component__ = __webpack_require__(697);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_common_chat_chat_component__ = __webpack_require__(696);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_common_video_video_component__ = __webpack_require__(698);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_common_search_users_search_users_component__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_common_search_rooms_search_rooms_component__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_pages_main_main_page__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_pages_home_home_page__ = __webpack_require__(465);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_9__components_layout_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_10__components_layout_footer_footer_component__["a" /* default */],
                __WEBPACK_IMPORTED_MODULE_11__components_layout_divider_divider_component__["a" /* default */],
                __WEBPACK_IMPORTED_MODULE_12__components_common_loading_loading_component__["a" /* default */],
                __WEBPACK_IMPORTED_MODULE_13__components_common_chat_chat_component__["a" /* default */],
                __WEBPACK_IMPORTED_MODULE_14__components_common_video_video_component__["a" /* default */],
                __WEBPACK_IMPORTED_MODULE_15__components_common_search_users_search_users_component__["a" /* default */],
                __WEBPACK_IMPORTED_MODULE_16__components_common_search_rooms_search_rooms_component__["a" /* default */],
                __WEBPACK_IMPORTED_MODULE_17__components_pages_main_main_page__["a" /* MainPage */],
                __WEBPACK_IMPORTED_MODULE_18__components_pages_home_home_page__["a" /* default */]
            ],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_15__components_common_search_users_search_users_component__["a" /* default */],
                __WEBPACK_IMPORTED_MODULE_16__components_common_search_rooms_search_rooms_component__["a" /* default */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_material__["a" /* MaterialModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_5__angular_flex_layout__["FlexLayoutModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_7__app_router__["a" /* routes */]
            ],
            providers: [],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(454);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_pages_main_main_page__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_pages_home_home_page__ = __webpack_require__(465);
/* unused harmony export router */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routes; });



var router = [
    { path: "", redirectTo: "main", pathMatch: "full" },
    { path: "main", component: __WEBPACK_IMPORTED_MODULE_1__components_pages_main_main_page__["a" /* MainPage */] },
    { path: "home", component: __WEBPACK_IMPORTED_MODULE_2__components_pages_home_home_page__["a" /* default */] }
];
var routes = __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* RouterModule */].forRoot(router, { useHash: true });
//# sourceMappingURL=app.router.js.map

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var EventEmitter = (function () {
    function EventEmitter() {
        this.events = new Map();
    }
    EventEmitter.prototype.on = function (key, action) {
        if (this.events.has(key)) {
            var actions = this.events.get(key);
            actions.push(action);
            this.events.set(key, actions);
        }
        else {
            this.events.set(key, new Array(action));
        }
    };
    EventEmitter.prototype.emit = function (key, args) {
        var firedEventActions = this.events.get(key);
        firedEventActions.forEach(function (element) {
            element(args);
        });
    };
    return EventEmitter;
}());
var instance = new EventEmitter();
/* harmony default export */ __webpack_exports__["a"] = instance;
//# sourceMappingURL=EventEmitter.js.map

/***/ }),

/***/ 695:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__common_helpers_EventEmitter__ = __webpack_require__(694);

var BaseComponent = (function () {
    function BaseComponent() {
        this.customEventListener = new Map();
    }
    BaseComponent.prototype.listen = function (key, action) {
        __WEBPACK_IMPORTED_MODULE_0__common_helpers_EventEmitter__["a" /* default */].on(key, action);
        this.customEventListener.set(key, action);
    };
    BaseComponent.prototype.emit = function (key) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        __WEBPACK_IMPORTED_MODULE_0__common_helpers_EventEmitter__["a" /* default */].emit(key, args);
    };
    BaseComponent.prototype.ngOnDestroy = function () {
        this.customEventListener.forEach(function (value, key) {
            // Events.emitter.off(key, value);
        });
    };
    return BaseComponent;
}());
/* harmony default export */ __webpack_exports__["a"] = BaseComponent;
//# sourceMappingURL=BaseComponent.js.map

/***/ }),

/***/ 696:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Chat = (function () {
    function Chat() {
        this.init();
    }
    Chat.prototype.init = function () {
        this.loggedUser = window.localStorage.getItem("user");
        this.isConnection = window.localStorage.getItem("isConnection");
        if (!this.isConnection || this.isConnection == "false") {
            RTCInit();
        }
    };
    Chat.prototype.sendMessage = function () {
        sendMessage(this.message, this.loggedUser);
    };
    Chat.prototype.shareFile = function () {
        shareFile();
    };
    Chat = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: "chat",
            template: __webpack_require__(870),
            styles: [__webpack_require__(857)]
        }), 
        __metadata('design:paramtypes', [])
    ], Chat);
    return Chat;
}());
/* harmony default export */ __webpack_exports__["a"] = Chat;
//# sourceMappingURL=chat.component.js.map

/***/ }),

/***/ 697:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__BaseComponent__ = __webpack_require__(695);
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Loading = (function (_super) {
    __extends(Loading, _super);
    function Loading() {
        _super.call(this);
        this.isHidden = true;
        this.initListener();
    }
    Loading.prototype.initListener = function () {
        var _this = this;
        _super.prototype.listen.call(this, "start-spinning", function () {
            _this.isHidden = false;
        });
        _super.prototype.listen.call(this, "stop-spinning", function () {
            _this.isHidden = true;
        });
    };
    Loading = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: "loading",
            template: __webpack_require__(871),
            styles: [__webpack_require__(858)]
        }), 
        __metadata('design:paramtypes', [])
    ], Loading);
    return Loading;
}(__WEBPACK_IMPORTED_MODULE_1__BaseComponent__["a" /* default */]));
/* harmony default export */ __webpack_exports__["a"] = Loading;
//# sourceMappingURL=loading.component.js.map

/***/ }),

/***/ 698:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Video = (function () {
    function Video() {
    }
    Video = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: "video",
            template: __webpack_require__(874),
            styles: [__webpack_require__(861)]
        }), 
        __metadata('design:paramtypes', [])
    ], Video);
    return Video;
}());
/* harmony default export */ __webpack_exports__["a"] = Video;
//# sourceMappingURL=video.component.js.map

/***/ }),

/***/ 699:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var Divider = (function () {
    function Divider() {
    }
    Divider = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: "divider",
            template: __webpack_require__(875),
            styles: [__webpack_require__(862)]
        }), 
        __metadata('design:paramtypes', [])
    ], Divider);
    return Divider;
}());
/* harmony default export */ __webpack_exports__["a"] = Divider;
//# sourceMappingURL=divider.component.js.map

/***/ }),

/***/ 700:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = (function () {
    function FooterComponent() {
        this.cssValidationImageUrl = "http://jigsaw.w3.org/css-validator/images/vcss-blue";
        this.cssValidationUrl = "http://jigsaw.w3.org/css-validator/check/referer";
    }
    FooterComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: "footer",
            template: __webpack_require__(876),
            styles: [__webpack_require__(863)]
        }), 
        __metadata('design:paramtypes', [])
    ], FooterComponent);
    return FooterComponent;
}());
/* harmony default export */ __webpack_exports__["a"] = FooterComponent;
//# sourceMappingURL=footer.component.js.map

/***/ }),

/***/ 701:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_material__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__common_search_users_search_users_component__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__common_search_rooms_search_rooms_component__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__common_services_UserService__ = __webpack_require__(186);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NavbarComponent = (function () {
    function NavbarComponent(userService, dialog) {
        this.userService = userService;
        this.dialog = dialog;
        this.loggedUser = window.localStorage.getItem("user");
    }
    NavbarComponent.prototype.openUserDialog = function () {
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_2__common_search_users_search_users_component__["a" /* default */]);
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    NavbarComponent.prototype.openRoomDialog = function () {
        var dialogRef = this.dialog.open(__WEBPACK_IMPORTED_MODULE_3__common_search_rooms_search_rooms_component__["a" /* default */]);
        dialogRef.afterClosed().subscribe(function (result) {
        });
    };
    NavbarComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_5" /* Component */])({
            selector: "navbar",
            template: __webpack_require__(877),
            styles: [__webpack_require__(864)],
            providers: [__WEBPACK_IMPORTED_MODULE_4__common_services_UserService__["a" /* default */], __WEBPACK_IMPORTED_MODULE_2__common_search_users_search_users_component__["a" /* default */]]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_4__common_services_UserService__["a" /* default */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_4__common_services_UserService__["a" /* default */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MdDialog */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_material__["d" /* MdDialog */]) === 'function' && _b) || Object])
    ], NavbarComponent);
    return NavbarComponent;
    var _a, _b;
}());
//# sourceMappingURL=navbar.component.js.map

/***/ }),

/***/ 702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 856:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, ".app-content {\n  display: block;\n  overflow: auto;\n  height: 100%; }\n\n.app-page-container {\n  min-height: calc(100% - 70px); }\n\n.space-between {\n  height: 20px; }\n\nbutton {\n  top: -1.5px;\n  outline: 0;\n  cursor: pointer;\n  position: relative;\n  display: inline-block;\n  background: 0;\n  border: 2px solid #e3e3e3;\n  padding: 20px 0;\n  font-size: 24px;\n  font-weight: 100;\n  line-height: 1;\n  text-transform: uppercase;\n  overflow: hidden;\n  transition: .3s ease;\n  bottom: 20px; }\n\nbutton:before {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  display: block;\n  background: #ed2553;\n  width: 30px;\n  height: 10px;\n  border-radius: 100%;\n  opacity: 0;\n  transition: .3s ease; }\n\nbutton:hover, button:active, button:focus {\n  border-color: #ed2553 !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 857:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, ".message {\n  border: 2px solid #ed2553;\n  border-radius: 2px;\n  width: 100%; }\n\n.read {\n  height: 275px;\n  margin-bottom: 5px;\n  overflow-y: scroll; }\n\nmd-input-container {\n  width: 100% !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 858:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, ".loading {\n  display: -ms-flexbox;\n  display: -webkit-box;\n  display: flex;\n  -ms-flex-pack: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -ms-flex-align: center;\n  -webkit-box-align: center;\n          align-items: center;\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n  z-index: 80;\n  overflow: hidden; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 859:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, "h3 {\n  color: #ed2553; }\n\nbutton {\n  top: -1.5px;\n  outline: 0;\n  cursor: pointer;\n  position: relative;\n  display: inline-block;\n  background: 0;\n  border: 2px solid #e3e3e3;\n  padding: 20px 0;\n  font-size: 24px;\n  font-weight: 100;\n  line-height: 1;\n  text-transform: uppercase;\n  overflow: hidden;\n  transition: .3s ease;\n  bottom: 20px;\n  margin-top: 35px; }\n\nbutton:before {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  display: block;\n  background: #ed2553;\n  width: 30px;\n  height: 10px;\n  border-radius: 100%;\n  opacity: 0;\n  transition: .3s ease; }\n\nbutton:hover, button:active, button:focus {\n  border-color: #ed2553 !important; }\n\n.container {\n  border: 2px solid #ed2553;\n  border-radius: 2px;\n  width: 100%;\n  height: 275px;\n  margin-bottom: 5px;\n  overflow-y: auto; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 860:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, "h3 {\n  color: #ed2553; }\n\nbutton {\n  top: -1.5px;\n  outline: 0;\n  cursor: pointer;\n  position: relative;\n  display: inline-block;\n  background: 0;\n  border: 2px solid #e3e3e3;\n  padding: 20px 0;\n  font-size: 24px;\n  font-weight: 100;\n  line-height: 1;\n  text-transform: uppercase;\n  overflow: hidden;\n  transition: .3s ease;\n  bottom: 20px;\n  margin-top: 35px; }\n\nbutton:before {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  display: block;\n  background: #ed2553;\n  width: 30px;\n  height: 10px;\n  border-radius: 100%;\n  opacity: 0;\n  transition: .3s ease; }\n\nbutton:hover, button:active, button:focus {\n  border-color: #ed2553 !important; }\n\n.container {\n  border: 2px solid #ed2553;\n  border-radius: 2px;\n  width: 100%;\n  height: 275px;\n  margin-bottom: 5px;\n  overflow-y: auto; }\n\n.icon {\n  width: 20px;\n  height: 20px; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 861:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 862:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, "div {\n  border-top: 1px solid;\n  border-top-color: #000;\n  width: 100%;\n  display: block;\n  margin: 0; }\n\n:host {\n  width: 100%; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 863:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, ":host {\n  position: absolute;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  display: none; }\n\n.footer-content {\n  height: 30px;\n  background-color: #ed2553;\n  color: white;\n  text-align: center;\n  padding: 15px 0; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 864:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, ".spacer {\n  -webkit-box-flex: 1;\n      -ms-flex: 1 1 auto;\n          flex: 1 1 auto; }\n\nspan {\n  padding: 0 15px;\n  cursor: pointer; }\n\nspan:hover {\n  color: rgba(90, 90, 90, 0.7); }\n\n.logo {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center; }\n\n.menu-item-toggle-icon {\n  vertical-align: middle; }\n\n.small-navbar {\n  background-color: #ed2553;\n  min-height: 28px;\n  font-size: 14px;\n  min-height: 32px;\n  font-size: 18px;\n  text-align: left;\n  padding: 0 16px;\n  box-shadow: 0 5px 5px -5px #333; }\n  .small-navbar span {\n    display: inline-block;\n    margin: 3px 5px;\n    color: #fff;\n    cursor: pointer;\n    padding: 0 10px; }\n  .small-navbar span:hover {\n    color: rgba(0, 0, 0, 0.7); }\n  .small-navbar .highlight-link {\n    color: #fbb63c; }\n  .small-navbar .highlight-link:hover {\n    color: #ef9c09; }\n  .small-navbar .mat-toolbar-row {\n    height: 24px; }\n\n.small-navigation .small-nav-item {\n  padding: 5px 0 5px 15px;\n  font-size: 20px;\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.small-navigation .small-nav-toggle-item {\n  font-family: Roboto, \"Helvetica Neue\", sans-serif; }\n\n.small-navigation .small-nav-toggle-item > button {\n  padding: 0 0 0 15px;\n  font-size: 20px; }\n\n.small-navigation .inner-menu {\n  padding-left: 35px !important;\n  background-color: #647b87; }\n\n.small-navigation .inner-menu > div {\n  padding: 5px 0; }\n\n.small-navigation div {\n  color: #fff;\n  cursor: pointer; }\n\n.small-navigation div:hover {\n  color: rgba(0, 0, 0, 0.7); }\n\n.small-navigation .toggle {\n  transform: rotateZ(90deg);\n  -webkit-transform: rotateZ(90deg);\n  -ms-transform: rotateZ(90deg); }\n\n.small-navigation .notoggle {\n  transform: rotateZ(270deg);\n  -webkit-transform: rotateZ(270deg);\n  -ms-transform: rotateZ(270deg); }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 865:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, ".content {\n  padding: 10px; }\n\nvideo {\n  border: 2px solid #ed2553;\n  border-radius: 2px;\n  height: 100px; }\n\ninput {\n  font-size: 50px; }\n\n.chat {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex; }\n\n.video {\n  width: 50%; }\n\n.datachannel {\n  width: 50%; }\n\nbutton {\n  top: -1.5px;\n  outline: 0;\n  cursor: pointer;\n  position: relative;\n  display: inline-block;\n  background: 0;\n  border: 2px solid #e3e3e3;\n  padding: 20px 0;\n  font-size: 24px;\n  font-weight: 100;\n  line-height: 1;\n  text-transform: uppercase;\n  overflow: hidden;\n  transition: .3s ease;\n  bottom: 20px; }\n\nbutton:before {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  display: block;\n  background: #ed2553;\n  width: 30px;\n  height: 10px;\n  border-radius: 100%;\n  opacity: 0;\n  transition: .3s ease; }\n\nbutton:hover, button:active, button:focus {\n  border-color: #ed2553 !important; }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 866:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(23)();
// imports


// module
exports.push([module.i, ".container {\n  top: 5%;\n  position: relative;\n  max-width: 35%;\n  width: 100%;\n  margin: 0 auto 200px; }\n\n.container.active .card:first-child {\n  background: #f2f2f2;\n  margin: 0 15px; }\n\n.container.active .card:nth-child(2) {\n  background: #fafafa;\n  margin: 0 10px; }\n\n.container.active .card.alt {\n  top: 20px;\n  right: 0;\n  width: 100%;\n  min-width: 100%;\n  height: auto;\n  border-radius: 5px;\n  padding: 60px 0 40px;\n  overflow: hidden; }\n\n.container.active .card.alt .toggle {\n  position: absolute;\n  top: 40px;\n  right: -70px;\n  box-shadow: none;\n  -webkit-transform: scale(10);\n  transform: scale(10);\n  transition: -webkit-transform .3s ease;\n  transition: transform .3s ease;\n  transition: transform .3s ease, -webkit-transform .3s ease; }\n\n.container.active .card.alt .title,\n.container.active .card.alt .input-container,\n.container.active .card.alt .button-container {\n  left: 0;\n  opacity: 1;\n  visibility: visible;\n  transition: .3s ease; }\n\n.container.active .card.alt .title {\n  transition-delay: .3s; }\n\n.container.active .card.alt .input-container {\n  transition-delay: .4s; }\n\n.container.active .card.alt .input-container:nth-child(2) {\n  transition-delay: .5s; }\n\n.container.active .card.alt .input-container:nth-child(3) {\n  transition-delay: .6s; }\n\n.container.active .card.alt .button-container {\n  transition-delay: .7s; }\n\n/* Card */\n.card {\n  position: relative;\n  background: #ffffff;\n  border-radius: 5px;\n  padding: 60px 0 40px 0;\n  box-sizing: border-box;\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);\n  transition: .3s ease; }\n\n.card:first-child {\n  background: #fafafa;\n  height: 10px;\n  border-radius: 5px 5px 0 0;\n  margin: 0 10px;\n  padding: 0; }\n\n.card .title {\n  position: relative;\n  z-index: 1;\n  border-left: 5px solid #ed2553;\n  margin: 0 0 35px;\n  padding: 10px 0 10px 50px;\n  color: #ed2553;\n  font-size: 32px;\n  font-weight: 600;\n  text-transform: uppercase; }\n\n.card .input-container {\n  position: relative;\n  margin: 0 60px 50px; }\n\n.card .input-container input {\n  outline: none;\n  z-index: 1;\n  position: relative;\n  background: none;\n  width: 100%;\n  height: 60px;\n  border: 0;\n  color: #212121;\n  font-size: 24px;\n  font-weight: 400; }\n\n.card .input-container input:focus ~ .bar:before, .card .input-container input:focus ~ .bar:after {\n  width: 50%; }\n\n.card .input-container label {\n  position: absolute;\n  top: 0;\n  left: 0;\n  color: #757575;\n  font-size: 24px;\n  font-weight: 300;\n  line-height: 60px;\n  transition: 0.2s ease; }\n\n.card .input-container .bar {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  background: #757575;\n  width: 100%;\n  height: 1px; }\n\n.card .input-container .bar:before, .card .input-container .bar:after {\n  position: absolute;\n  background: #ed2553;\n  width: 0;\n  height: 2px;\n  transition: .2s ease; }\n\n.card .input-container .bar:before {\n  left: 50%; }\n\n.card .input-container .bar:after {\n  right: 50%; }\n\n.card .button-container {\n  margin: 0 60px;\n  text-align: center; }\n\n.card .button-container button {\n  top: -1.5px;\n  outline: 0;\n  cursor: pointer;\n  position: relative;\n  display: inline-block;\n  background: 0;\n  border: 2px solid #e3e3e3;\n  padding: 20px 0;\n  font-size: 24px;\n  font-weight: 600;\n  line-height: 1;\n  text-transform: uppercase;\n  overflow: hidden;\n  transition: .3s ease; }\n\n.card .button-container button span {\n  position: relative;\n  z-index: 1;\n  color: #ddd;\n  transition: .3s ease; }\n\n.card .button-container button:before {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  display: block;\n  background: #ed2553;\n  width: 30px;\n  height: 30px;\n  border-radius: 100%;\n  margin: -15px 0 0 -15px;\n  opacity: 0;\n  transition: .3s ease; }\n\n.card .button-container button:hover, .card .button-container button:active, .card .button-container button:focus {\n  border-color: #ed2553; }\n\n.card .button-container button:hover span, .card .button-container button:active span, .card .button-container button:focus span {\n  color: #ed2553; }\n\n.card .button-container button:active:before, .card .button-container button:focus:before {\n  opacity: 1;\n  -webkit-transform: scale(10);\n  transform: scale(10); }\n\n.card .footer {\n  margin: 40px 0 0;\n  color: #d3d3d3;\n  font-size: 24px;\n  font-weight: 300;\n  text-align: center; }\n\n.card .footer a {\n  color: inherit;\n  text-decoration: none;\n  transition: .3s ease; }\n\n.card .footer a:hover {\n  color: #bababa; }\n\n.card.alt {\n  position: absolute;\n  top: 40px;\n  right: -70px;\n  z-index: 10;\n  width: 140px;\n  height: 140px;\n  background: none;\n  border-radius: 100%;\n  box-shadow: none;\n  padding: 0;\n  transition: .3s ease; }\n\n.card.alt .toggle {\n  position: relative;\n  background: #ed2553;\n  width: 140px;\n  height: 140px;\n  border-radius: 100%;\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);\n  color: #ffffff;\n  font-size: 58px;\n  line-height: 140px;\n  text-align: center;\n  cursor: pointer; }\n\n.card.alt .toggle:before {\n  display: inline-block;\n  font: normal normal normal 14px/1 FontAwesome;\n  font-size: inherit;\n  text-rendering: auto;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-transform: translate(0, 0);\n  transform: translate(0, 0); }\n\n.card.alt .title,\n.card.alt .input-container,\n.card.alt .button-container {\n  left: 100px;\n  opacity: 0;\n  visibility: hidden; }\n\n.card.alt .title {\n  position: relative;\n  border-color: #ffffff;\n  color: #ffffff; }\n\n.card.alt .title .close {\n  cursor: pointer;\n  position: absolute;\n  top: 0;\n  right: 60px;\n  display: inline;\n  color: #ffffff;\n  font-size: 58px;\n  font-weight: 400; }\n\n.card.alt .input-container input {\n  color: #ffffff; }\n\n.card.alt .input-container input:focus ~ label {\n  color: #ffffff; }\n\n.card.alt .input-container input:focus ~ .bar:before, .card.alt .input-container input:focus ~ .bar:after {\n  background: #ffffff; }\n\n.card.alt .input-container input:valid ~ label {\n  color: #ffffff; }\n\n.card.alt .input-container label {\n  color: rgba(255, 255, 255, 0.8); }\n\n.card.alt .input-container .bar {\n  background: rgba(255, 255, 255, 0.8); }\n\n.card.alt .button-container button {\n  width: 100%;\n  background: #ffffff;\n  border-color: #ffffff; }\n\n.card.alt .button-container button span {\n  color: #ed2553; }\n\n.card.alt .button-container button:hover {\n  background: rgba(255, 255, 255, 0.9); }\n\n.card.alt .button-container button:active:before, .card.alt .button-container button:focus:before {\n  display: none; }\n\n/* Keyframes */\n@-webkit-keyframes buttonFadeInUp {\n  0% {\n    bottom: 30px;\n    opacity: 0; } }\n\n@keyframes buttonFadeInUp {\n  0% {\n    bottom: 30px;\n    opacity: 0; } }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 869:
/***/ (function(module, exports) {

module.exports = "<div class=\"app-content\">\r\n  <div class=\"app-page-container\">\r\n    <router-outlet></router-outlet>\r\n  </div>\r\n  <div class=\"space-between\"></div>\r\n  <footer></footer>\r\n</div>\r\n<loading></loading>"

/***/ }),

/***/ 870:
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <p>\r\n        Write to room here!\r\n    </p>\r\n    <div id=\"chat-output\"class=\"read message\">\r\n\r\n    </div>\r\n    <div class=\"write message\">\r\n        <md-input-container>\r\n            <textarea mdInput id=\"input-text-chat\" placeholder=\"Type here...\" [(ngModel)]=\"message\" name=\"message\"></textarea>\r\n        </md-input-container>\r\n        <button md-raised-button (click)=\"sendMessage()\">Send</button>\r\n        <button md-raised-button (click)=\"shareFile()\">Add File</button>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 871:
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" [hidden]=\"isHidden\">\r\n    <md-spinner color=\"primary\"></md-spinner>\r\n    <div class=\"cdk-overlay-backdrop cdk-overlay-dark-backdrop cdk-overlay-backdrop-showing\"></div>\r\n</div>"

/***/ }),

/***/ 872:
/***/ (function(module, exports) {

module.exports = "<h3 (click)=\"getRooms()\">\r\n    Rooms\r\n</h3>\r\n<div>\r\n    <md-input-container>\r\n        <input mdInput type=\"text\" [(ngModel)]=\"searchData\" required placeholder=\"Room Name\" />\r\n    </md-input-container>\r\n    <br/>\r\n    <button md-raised-button (click)=\"getRoom()\">Search</button>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <md-list>\r\n        <md-list-item *ngFor=\"let room of rooms\">\r\n            <p md-line>{{room.name}}</p>\r\n        </md-list-item>\r\n        <h3 md-subheader>We found {{rooms.length}} results</h3>\r\n    </md-list>\r\n</div>"

/***/ }),

/***/ 873:
/***/ (function(module, exports) {

module.exports = "<h3 (click)=\"getUsers()\">\r\n    Users\r\n</h3>\r\n<div>\r\n    <md-input-container>\r\n        <input mdInput type=\"text\" [(ngModel)]=\"searchData\" required placeholder=\"Username\" />\r\n    </md-input-container>\r\n    <br/>\r\n    <button md-raised-button  (click)=\"getUser()\">Search</button>\r\n</div>\r\n\r\n<div class=\"container\">\r\n    <md-list>\r\n        <md-list-item *ngFor=\"let user of users\">\r\n            <div class=\"icon\" [style.backgroundColor]=\"user.color\"></div>\r\n            <p md-line>{{user.username}}</p>\r\n        </md-list-item>\r\n        <h3 md-subheader>We found {{users.length}} results</h3>\r\n    </md-list>\r\n</div>"

/***/ }),

/***/ 874:
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ 875:
/***/ (function(module, exports) {

module.exports = "<div></div>"

/***/ }),

/***/ 876:
/***/ (function(module, exports) {

module.exports = "<div class=\"footer-content\">\r\n    <p>&copy; WebRTC Client</p>\r\n</div>"

/***/ }),

/***/ 877:
/***/ (function(module, exports) {

module.exports = "<div class=\"small-navbar\">\r\n    <span>Hello, {{loggedUser}}!</span>\r\n    <span class=\"spacer\"></span>\r\n    <span (click)=\"openRoomDialog()\">Rooms</span>\r\n    <span (click)=\"openUserDialog()\">Users</span>\r\n</div>"

/***/ }),

/***/ 878:
/***/ (function(module, exports) {

module.exports = "<navbar></navbar>\r\n<div class=\"content\">\r\n    <div fxFlex fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-between center\" class=\"open-join-bar\">\r\n        <div fxFlex=\"50\">\r\n            <md-input-container>\r\n                <input mdInput type=\"text\" [(ngModel)]=\"roomName\" required placeholder=\"Room Name\" />\r\n            </md-input-container>\r\n        </div>\r\n        <div fxFlex=\"50\">\r\n            <md-checkbox class=\"example-margin\" [(ngModel)]=\"isPrivate\">Private</md-checkbox>\r\n            <button md-raised-button (click)=\"openRoom()\">Open</button>\r\n            <button md-raised-button (click)=\"joinRoom()\">Join</button>\r\n        </div>\r\n    </div>\r\n    <div fxFlex fxLayout=\"column\" fxLayout.gt-sm=\"row\" fxLayoutAlign=\"space-between center\" class=\"chat\">\r\n        <div fxFlex=\"50\" class=\"video\" id=\"videos-container\">\r\n        </div>\r\n        <div fxFlex=\"50\" class=\"datachannel\">\r\n            <chat></chat>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ 879:
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" [ngClass]=\"getStyle()\">\r\n  <div class=\"card\"></div>\r\n  <div class=\"card\">\r\n    <h1 class=\"title\">Login</h1>\r\n    <div class=\"input-container\">\r\n      <md-input-container>\r\n        <input mdInput type=\"text\" [(ngModel)]=\"loginData.username\" required placeholder=\"Username\" />\r\n      </md-input-container>\r\n    </div>\r\n    <div class=\"input-container\">\r\n      <md-input-container>\r\n        <input mdInput type=\"password\" [(ngModel)]=\"loginData.password\" required placeholder=\"Password\" />\r\n      </md-input-container>\r\n    </div>\r\n    <div class=\"button-container\">\r\n      <button md-raised-button (click)=\"login()\"><span>Login</span></button>\r\n    </div>\r\n  </div>\r\n  <div class=\"card alt\">\r\n    <div class=\"toggle\" (click)=\"showActiveClass = !showActiveClass;\">\r\n      <i class=\"material-icons\">perm_identity</i>\r\n    </div>\r\n    <h1 class=\"title\">Register\r\n      <div class=\"close\" (click)=\"showActiveClass = !showActiveClass;\">\r\n        <i class=\"material-icons\">close</i>\r\n      </div>\r\n    </h1>\r\n    <div class=\"input-container\">\r\n      <md-input-container>\r\n        <input mdInput type=\"text\" [(ngModel)]=\"registerData.username\" placeholder=\"Username\" required/>\r\n      </md-input-container>\r\n    </div>\r\n    <div class=\"input-container\">\r\n      <md-input-container>\r\n        <input mdInput type=\"password\" [(ngModel)]=\"registerData.password\" placeholder=\"Password\" required/>\r\n      </md-input-container>\r\n    </div>\r\n    <div class=\"input-container\">\r\n      <md-input-container>\r\n        <input mdInput type=\"password\" [(ngModel)]=\"registerData.repeatPassword\" placeholder=\"Repeat Password\" required/>\r\n      </md-input-container>\r\n    </div>\r\n    <div class=\"button-container\">\r\n      <button md-raised-button (click)=\"register()\">Next</button>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ 922:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(518);


/***/ })

},[922]);
//# sourceMappingURL=main.bundle.js.map