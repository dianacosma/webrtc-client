var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;
var url = "mongodb://localhost:27017/webrtcdb";

// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   console.log("Database created!");
//   db.close();
// });

// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   db.createCollection("users", function(err, res) {
//     if (err) throw err;
//     console.log("Table created!");
//     db.close();
//   });
// });

// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   db.createCollection("rooms", function(err, res) {
//     if (err) throw err;
//     console.log("Table created!");
//     db.close();
//   });
// });

// MongoClient.connect(url, function (err, db) {
//   if (err) throw err;
//   var myobj = [
//     {
//       username: "diana",
//       password: "123456",
//       color: "ffd1f6"
//     },
//     {
//       username: "alex",
//       password: "654321",
//       color: "9ed8ff"
//     },
//     {
//       username: "forest",
//       password: "123456",
//       color: "1d5324"
//     }
//   ];
//   db.collection("users").insertMany(myobj, function (err, res) {
//     if (err) throw err;
//     console.log("Number of records inserted: " + res.insertedCount);
//     db.close();
//   });
// });

// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   db.collection("rooms").drop();
//   db.close();
// });

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  db.collection("rooms").find({room: 'a'}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    db.close();
  });
});

// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   db.collection("users").find({username: "diana"}).toArray(function(err, result) {
//     if (err) throw err;
//     console.log(result);
//     db.close();
//   });
// });

// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   var myquery = { username: 'diana' };
//   db.collection("users").deleteOne(myquery, function(err, obj) {
//     if (err) throw err;
//     console.log("1 document deleted");
//     db.close();
//   });
// });
